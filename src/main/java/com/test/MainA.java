package com.test;

import com.test.config.Config;
import com.test.dao.BookDao;
import com.test.dao.BookJpaDao;
import com.test.domain.Author;
import com.test.domain.Book;
import com.test.service.BookService;
import com.test.service.BookServiceImpl;
import com.test.util.HibernateUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by olegls2000 on 10/13/2015.
 */
public class MainA {
    public static void main(String[] args) {

        //ApplicationContext context = new ClassPathXmlApplicationContext("app-context.xml");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        BookService bookService = (BookService) context.getBean("bookServiceImpl");

        Author author = new Author();
        author.setFirstName("Nik");
        author.setSecondName("Nik");

        Book book = new Book();
        book.setName("Bask555erville");
        book.setPrice(new BigDecimal(99900.55d));
        book.setAuthor(author);

        Book book1 = new Book();
        book1.setName("Bla");
        book1.setPrice(new BigDecimal(5800.55d));
        book1.setAuthor(author);

        Set<Book> bookSet = new HashSet<Book>();
        bookSet.add(book);
        bookSet.add(book1);
        author.setBooks(bookSet);
        bookService.addAuthor(author);

        bookService.addBook(book);
        bookService.addBook(book1);

        Book byName = bookService.findBookByName("Bla");
        System.out.println(byName);

        BigDecimal maxPrice = bookService.findMaxPrice();
        System.out.println(maxPrice);

        HibernateUtil.shutdown();

    }
}
