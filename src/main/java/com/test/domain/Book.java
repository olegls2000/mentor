package com.test.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by olegls2000 on 10/21/2015.
 */

@NamedQueries({
        @NamedQuery(name = "Book.findBookByPrice",
                query = "from Book b where b.price < :price"),
        @NamedQuery(name = "Book.findBookByName",
        query = "from Book b where b.name = :name")
})
@NamedNativeQueries({
        @NamedNativeQuery( name = "Book.findMaxPrice",
                query = "select max(price) from book"),
        @NamedNativeQuery( name = "Book.findMinPrice",
                query = "select min(price) from book")
})
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 500)
    private String name;

    private BigDecimal price;

    @ManyToOne()
    private Author author;

    public Long getId() {
        return id;
    }

    public void setId(Long employeeId) {
        this.id = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
