package com.test.domain;
///sdfdsfdsf

import javax.persistence.*;
import java.util.Set;

/**
 * Created by olegls2000 on 10/29/2015.
 */

@NamedQuery(name = "Author.findAuthorByName",
        query = "from Author a where a.firstName = :firstName")

@Entity
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String secondName;

    @OneToMany(mappedBy = "author")
    Set<Book> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }
}
