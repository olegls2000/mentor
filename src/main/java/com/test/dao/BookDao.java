package com.test.dao;

import com.test.domain.Book;

import java.math.BigDecimal;

/**
 * Created by olegls2000 on 10/21/2015.
 */
public interface BookDao {
    void add (Book book);
    Book findBookByName(String name);
    BigDecimal findMaxPrice();
}
