package com.test.dao;

import com.test.domain.Author;

/**
 * Created by olegls2000 on 11/4/2015.
 */
public interface AuthorDao {
    Author getAuthorByName(String firstName);
    void addAuthor(Author author);
}
