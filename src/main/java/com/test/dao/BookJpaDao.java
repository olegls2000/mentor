package com.test.dao;

import com.test.domain.Book;
import com.test.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by olegls2000 on 10/21/2015.
 */
@Repository
@Qualifier(value = "bookjpaDao")
public class BookJpaDao implements BookDao {

    public BookJpaDao() {
        System.out.println("Bean BookJpaDao was initialised");
    }

    public void add(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
    }

    public Book findBookByName(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("Book.findBookByName")
                    .setString("name", name);
            List<Book> result = query.list();
            return result.get(0);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
        return null;
    }

    public BigDecimal findMaxPrice() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("Book.findMaxPrice");
            BigDecimal maxPrice = (BigDecimal) query.uniqueResult();
            return maxPrice;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
        return null;
    }
}
