package com.test.dao;

import com.test.domain.Book;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by olegls2000 on 10/22/2015.
 */
@Repository
@Qualifier(value = "bookStubDao")
public class BookStubDao implements BookDao {

    private List<Book> db;

    public BookStubDao() {
        db = new ArrayList<Book>();
    }

    public void add(Book book) {
        db.add(book);
    }

    public Book findBookByName(String name) {
        return null;
    }

    public BigDecimal findMaxPrice() {
        return null;
    }
}
