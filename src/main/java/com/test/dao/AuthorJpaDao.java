package com.test.dao;

import com.test.domain.Author;
import com.test.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by olegls2000 on 11/4/2015.
 */
@Repository
public class AuthorJpaDao implements AuthorDao {

    public Author getAuthorByName(String firstName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("Author.findAuthorByName")
                    .setString("firstName", firstName);
            List<Author> result = query.list();
            return result.get(0);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
        return null;
    }

    public void addAuthor(Author author) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(author);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
    }
}
