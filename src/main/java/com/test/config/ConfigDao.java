package com.test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by olegls2000 on 11/17/2015.
 */
@Configuration
@ComponentScan("com.test.dao")
public class ConfigDao {

}
