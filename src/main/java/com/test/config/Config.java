package com.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by olegls2000 on 11/17/2015.
 */
@Configuration
@Import(value = {ConfigDao.class, ConfigService.class})
public class Config {
}
