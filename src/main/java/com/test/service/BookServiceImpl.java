package com.test.service;

import com.test.dao.*;
import com.test.domain.Author;
import com.test.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.math.BigDecimal;

/**
 * Created by olegls2000 on 10/22/2015.
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    @Qualifier(value = "bookStubDao")
    private BookDao bookDao;

    private AuthorDao authorDao;

    public BookServiceImpl() {
        System.out.println("Bean BookServiceImpl was initialised");
    }

    public void addBook(Book book) {
        book.setPrice(book.getPrice().add(new BigDecimal(200)));
        bookDao.add(book);
    }

    public Book findBookByName(String name) {
        return bookDao.findBookByName(name);
    }

    public BigDecimal findMaxPrice() {
        return bookDao.findMaxPrice();
    }

    public void addAuthor(Author author) {
        authorDao.addAuthor(author);
    }



}
