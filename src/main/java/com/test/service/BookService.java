package com.test.service;

import com.test.domain.Author;
import com.test.domain.Book;

import java.math.BigDecimal;

/**
 * Created by olegls2000 on 10/22/2015.
 */
public interface BookService {
    void addBook (Book book);
    Book findBookByName(String name);
    BigDecimal findMaxPrice();
    void addAuthor(Author author);
}
